using System;
using System.Collections.Generic;
using System.Text;

namespace regreplace
{
    class InputParams
    {
        List<string> paramNames = new List<string>(new string [] {"-f", "-r", "-s", "-e", "-t", "-o", "--format"}); 
        public InputParams(string[] args)
        {            
            int index = 0;
            foreach (string arg in args)
            {
                if (paramNames.Contains(arg) && args.Length - 1 > index)
                { 
                    switch (arg)
                    {
                        case "-f": SearchStr = args[index + 1]; // find
                            break;
                        case "-r": ReplaceStr = args[index + 1]; // replace
                            break;
                        case "-s": SourcePath = args[index + 1]; // source
                            break;
                        case "-t": TestOnly = true; // test
                            break;
                        case "-e": FileExpression = args[index + 1]; // expression
                            break;
                        case "-o": Output = args[index + 1]; // expression
                            break;
                        case "--format" : this.OutputFormat = FromString(args[index + 1]); // --format FileName|RowNum|ColNum|SourceRow|ResultRow
                            break;
                         
                    }
                }
                index++;
            }
            
        }
        
        public int FromString(string format)
        {
        	int result = 0;
        	string [] formats = format.Split('|');
        	foreach (string f in formats)
        	{
        		switch(f)
        		{
        			case "FileName" :
        				result = result | (int)regreplace.OutputFormat.FileName;
        				break;
        			case "RowNum" :
        				result = result | (int)regreplace.OutputFormat.RowNum;
        				break;
        			case "ColNum" :
        				result = result | (int)regreplace.OutputFormat.ColNum;
        				break;	
        			case "SourceRow" :
        				result = result | (int)regreplace.OutputFormat.SourceRow;
        				break;	
        			case "ResultRow" :
        				result = result | (int)regreplace.OutputFormat.ResultRow;
        				break;	
        				
        		}
        	}
        	
        	return result;
        	
        }

        public string SourcePath;
        public string SearchStr;
        public string ReplaceStr;
        public bool TestOnly;
        public string FileExpression;
        public string Output;
        public int OutputFormat;
    }
}
