using System;
using System.Collections.Generic;
using System.Text;

namespace regreplace
{
    struct ReplaceMatch
    {
        public string FileName;
        public int RowNum;
        public int ColNum;
        public string SourceRow;
        public string ResultRow;
    }
}
