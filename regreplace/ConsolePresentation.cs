using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace regreplace
{
	class ConsolePresentation : IDisposable
    {
        private ConsolePresentation()
        {        	
        }

        private static ConsolePresentation fInstance;
        public static ConsolePresentation Instance
        {
            get
            {
                if (fInstance == null) fInstance = new ConsolePresentation();
                return fInstance;
            }
        }    
        
        private StreamWriter w;
        private string outputFilename;
        public string OutputFile
        {
        	get
        	{
        		return outputFilename;        			
        	}
        	
        	set
        	{
        		if (w != null)
        			w.Close();
        		w = new StreamWriter(value);        		
        		Console.SetOut(w);        		
        		outputFilename = value;
        	}
        }
        
        public int OutputFormat;
        
        public void ResetConsoleOutput()
        {
        	if (w != null) 
        	{
        		w.Flush();
        		w.Close();
        	}
        	StreamWriter w1 = new StreamWriter(Console.OpenStandardOutput());
        	w1.AutoFlush = true;
        	Console.SetOut(w1);
        	
        }

        public void Show(ReplaceMatch[] results)
        {
            string fileName = string.Empty;
            int i =0;
            foreach (ReplaceMatch match in results)
            {
            	if (fileName != match.FileName && (this.OutputFormat&(int)regreplace.OutputFormat.FileName) != 0)
                {
                	Console.WriteLine("[{0}]",i+1);
                	Console.ForegroundColor = ConsoleColor.DarkYellow;
                	Console.WriteLine("File : ");
                	Console.ForegroundColor = ConsoleColor.Gray;
                    Console.WriteLine(match.FileName);
                }
            	if ((this.OutputFormat&(int)regreplace.OutputFormat.RowNum) != 0)
                	Console.WriteLine("Row : {0} Column : {1} ", match.RowNum + 1, match.ColNum + 1);
            	if ((this.OutputFormat&(int)regreplace.OutputFormat.SourceRow) != 0)
            	{
            		Console.ForegroundColor = ConsoleColor.White;
            		Console.WriteLine("Old : {0}", match.SourceRow);
            	}
                
            	if ((this.OutputFormat&(int)regreplace.OutputFormat.ResultRow) != 0)
            	{
	                Console.ForegroundColor = ConsoleColor.Gray;
	                Console.WriteLine("New : {0}", match.ResultRow);                
            	}
                i++;
            }
            
        }     
        
        public void Dispose()
        {
        	if(w != null)
        		w.Close();
        }
        
        
    }
}
