/*
 * Created by SharpDevelop.
 * User: epirogov
 * Date: 7/20/2006
 * Time: 12:43 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;

namespace regreplace
{
	/// <summary>
	/// Description of OutputFormat.
	/// </summary>
	public enum OutputFormat {FileName = 1, RowNum = 2, ColNum = 4, SourceRow = 8, ResultRow = 16};
	
}
