using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace regreplace
{
    class Program
    {
        static void Main(string[] args)
        {
            InputParams input = new InputParams(args);
            ConsolePresentation.Instance.OutputFormat = input.OutputFormat;
            ReplaceMatch [] results = CoreRegReplace.Insatance.Replace(input);           
            
            if (input.Output != null)
            {           	
            	ConsolePresentation.Instance.OutputFile = input.Output;
            }
            ConsolePresentation.Instance.Show(results);
            
            ConsolePresentation.Instance.ResetConsoleOutput();
            Console.WriteLine("Completed");       
            Console.ReadLine();
        }
        
        //-t -s E:\Share\ARISE_WebPublish -f http://www.livejournal.com/users/epirogov/ -e .+\.html -r http://arisetheme.blogspot.com/
        //-t -s D:\Pirogov\Clienteer_1\ClienteerDatabase\DatabaseScripts\Scripts -e .+\.txt -f fn_get_concomp_name_byID -r http://arisetheme.blogspot.com/ -o c:\info.txt
        //-t -s D:\Pirogov\Clienteer_1\ClienteerDatabase\DatabaseScripts\Scripts -e .+\.txt -f fn_get_contact_company_name[\(\s] -r fn_get_concomp_name_byID
        //-t -s D:\Pirogov\Clienteer_1\ClienteerDatabase\DatabaseScripts\Scripts -e .+\.txt -f fn_get_contact_company_name2[\(\s] -r fn_get_contact_company_name2 -o c:\fn_get_contact_company_name2.txt
        //-t -s D:\Pirogov\Clienteer_1\ClienteerDatabase\DatabaseScripts\Scripts -e .+\.txt -f fn_get_contact_company_name2[\(\s] -r fn_get_contact_company_name2 -o c:\fn_get_contact_company_name2.txt --format  FileName|RowNum|ColNum
    }
}
