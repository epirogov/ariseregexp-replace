using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace regreplace
{
    class DirectoryAnalyser
    {
        private DirectoryAnalyser()
        {
        }

        private static DirectoryAnalyser fInstance;
        public static DirectoryAnalyser Instance
        {
            get
            {
                if (fInstance == null) fInstance = new DirectoryAnalyser();
                return fInstance;
            }
        }

        public string[] GetSubdirFiles(string dir)
        {
            ArrayList files = new ArrayList();
            Stack<string> subdirs = new Stack<string>();
            subdirs.Push(dir);

            while (subdirs.Count > 0)
            {
                string curdir = subdirs.Pop();
                string[] currFiles = Directory.GetFiles(curdir);
                foreach (string file in currFiles) files.Add(file);
                string[] currSubdir = Directory.GetDirectories(curdir);
                foreach (string subdir in currSubdir) subdirs.Push(subdir);
            }
            return (string[])files.ToArray(typeof(string));
        }
    }
}
