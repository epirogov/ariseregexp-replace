using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Text;

namespace regreplace
{
    class CoreRegReplace
    {
        private CoreRegReplace()
        {
        }

        private static CoreRegReplace fInsatance;
        
        public static CoreRegReplace Insatance
        {
            get
            {
                if (fInsatance == null) fInsatance = new CoreRegReplace();
                return fInsatance;
            }
            set
            {
            }
        }

        public ReplaceMatch[] Replace(InputParams param)
        {
            ArrayList results = new ArrayList();
            if (Directory.Exists(param.SourcePath))
            {
                string[] files = DirectoryAnalyser.Instance.GetSubdirFiles(param.SourcePath);
                Regex regex = new Regex(param.SearchStr);
                Regex expRegex = null;
                if (param.FileExpression != null) expRegex = new Regex(param.FileExpression);
                foreach (string file in files)
                {
                	if (expRegex == null || expRegex.Match(file).Success)
                	{
                		string fullText = string.Empty;
                		try
                		{
		                    StreamReader r = new StreamReader(file);
		                    fullText = r.ReadToEnd();
		                    r.Close();
                		}
                		catch(IOException)
                		{
                		}
	
	                    MatchCollection c = regex.Matches(fullText);
	                    foreach (Match m in c)
	                    {
	                        ReplaceMatch item = new ReplaceMatch();
	                        TextPosition t = GetTextPosition(fullText,m.Index);
	                        item.FileName = file;
	                        item.RowNum = t.Row;
	                        item.ColNum = t.Column;
	                        item.SourceRow = GetTextRow(fullText,t.Row);
	                        item.ResultRow = regex.Replace(item.SourceRow, param.ReplaceStr);
	                        results.Add(item);
	                    }
	                    
	                    if (!param.TestOnly)
		                    try
		                    {
			                    StreamWriter w = new StreamWriter(file);
			                    w.Write(regex.Replace(fullText, param.ReplaceStr));
			                    w.Close();
		                    }
		                    catch(IOException)
	                		{	                    
		                    }
                	
                	}
                }                
            }
            return (ReplaceMatch[])results.ToArray(typeof(ReplaceMatch));
        }

        private TextPosition GetTextPosition(string fulltext, int sumbolIndex)
        {
            TextPosition t = new TextPosition();
            Regex r = new Regex("\\n");
            MatchCollection m1 = r.Matches(fulltext);
            MatchCollection m2 = r.Matches(fulltext,sumbolIndex);
            Match prev = null;

            if (m1.Count > 0 && m2.Count > 0)
            {
                t.Row = m1.Count - m2.Count;

                foreach (Match m in m1)
                {
                    if (m.Index == m2[0].Index) break;
                    prev = m;
                }
                if (prev == null) t.Column = sumbolIndex;
                else t.Column = sumbolIndex - prev.Index;
            }
            else if (m1.Count > 0)
            {
                t.Row = m1.Count;
                t.Column = sumbolIndex - m1[m1.Count - 1].Index;
            }
            else
            {
                t.Row = 0;
                t.Column = sumbolIndex;
            }

            return t;
        }

        private string GetTextRow(string fulltext, int rowIndex)
        {
            Regex r = new Regex(".+\\n|$");
            MatchCollection ms = r.Matches(fulltext);

            return (ms.Count > rowIndex) ? ms[rowIndex].Value : null;           
        }
    }
}
